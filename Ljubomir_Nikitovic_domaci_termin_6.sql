-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: TDbaza1
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Drzava`
--

DROP TABLE IF EXISTS `Drzava`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Drzava` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kod` varchar(3) NOT NULL,
  `naziv` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Drzava`
--

LOCK TABLES `Drzava` WRITE;
/*!40000 ALTER TABLE `Drzava` DISABLE KEYS */;
INSERT INTO `Drzava` VALUES (1,'RS','Srbija'),(2,'BA','Bosna i Hercegovina'),(3,'H','Modjarska'),(4,'HR','Hrvatska'),(5,'MNE','Crna Gora'),(6,'A','Austrija');
/*!40000 ALTER TABLE `Drzava` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Mesto`
--

DROP TABLE IF EXISTS `Mesto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Mesto` (
  `idMesto` int(11) NOT NULL AUTO_INCREMENT,
  `ptt` varchar(10) NOT NULL,
  `naziv` varchar(100) NOT NULL,
  `drzavaFK` int(11) DEFAULT NULL,
  PRIMARY KEY (`idMesto`),
  KEY `fk_Mesto_1_idx` (`drzavaFK`),
  CONSTRAINT `fk_Mesto_1` FOREIGN KEY (`drzavaFK`) REFERENCES `Drzava` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Mesto`
--

LOCK TABLES `Mesto` WRITE;
/*!40000 ALTER TABLE `Mesto` DISABLE KEYS */;
INSERT INTO `Mesto` VALUES (1,'21000','Novi Sad',1),(2,'11000','Beograd',1),(3,'10000','Zagreb',4),(4,'1000','Bec',6),(5,'21000','Split',NULL),(6,'81000','Podgorica',NULL),(7,'82000','Bar',5),(8,'71000','Sarajevo',2);
/*!40000 ALTER TABLE `Mesto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Student`
--

DROP TABLE IF EXISTS `Student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(50) NOT NULL,
  `prezime` varchar(50) NOT NULL,
  `indeks` varchar(12) NOT NULL,
  `mestoID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Student_mesto` (`mestoID`),
  CONSTRAINT `fk_Student_1` FOREIGN KEY (`mestoID`) REFERENCES `Mesto` (`idMesto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Student`
--

LOCK TABLES `Student` WRITE;
/*!40000 ALTER TABLE `Student` DISABLE KEYS */;
INSERT INTO `Student` VALUES (1,'Stevan','Stevanovic','I 1/2020',1),(2,'Petar','Petrovic','I 2/2020',4),(3,'Stojan','Stojanovic','I 3/2020',NULL),(4,'Bojan','Bojanic','I 4/2020',NULL),(5,'Ana','Stankovic','I 5/2020',1);
/*!40000 ALTER TABLE `Student` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-21 21:00:58
